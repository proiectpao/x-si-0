/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.interfaces;

import game.GameManager;

/**
 *
 * @author Andrei
 */
public class GameMenuInterface extends javax.swing.JPanel {

    /**
     * Creates new form GameMenuInterface
     */
    public GameMenuInterface(GameManager game,int won,int lost,int draws) {
        initComponents();
        this.game = game;
        wonValueLabel.setText(won+"");
        lostValueLabel.setText(lost+"");
        drawsValueLabel.setText(draws+"");
        if (won != 0)
            winPercValueLabel.setText(String.format("%.2f%%",(double)won/(won+lost+draws)*100));
        else
            winPercValueLabel.setText("0%");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        titleLabel = new javax.swing.JLabel();
        newGameButton = new javax.swing.JButton();
        loadGameButton = new javax.swing.JButton();
        statisticsPane = new javax.swing.JPanel();
        wonLabel = new javax.swing.JLabel();
        lostLabel = new javax.swing.JLabel();
        drawsLabel = new javax.swing.JLabel();
        winPercLabel = new javax.swing.JLabel();
        wonValueLabel = new javax.swing.JLabel();
        lostValueLabel = new javax.swing.JLabel();
        drawsValueLabel = new javax.swing.JLabel();
        winPercValueLabel = new javax.swing.JLabel();

        titleLabel.setFont(new java.awt.Font("Noto Sans", 3, 48)); // NOI18N
        titleLabel.setForeground(new java.awt.Color(2, 19, 77));
        titleLabel.setText("Tic Tac Toe");

        newGameButton.setText("New game");
        newGameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newGameButtonActionPerformed(evt);
            }
        });

        loadGameButton.setText("Load game");
        loadGameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadGameButtonActionPerformed(evt);
            }
        });

        wonLabel.setText("Won:");

        lostLabel.setText("Lost:");

        drawsLabel.setText("Draws:");

        winPercLabel.setText("Win percentage:");

        javax.swing.GroupLayout statisticsPaneLayout = new javax.swing.GroupLayout(statisticsPane);
        statisticsPane.setLayout(statisticsPaneLayout);
        statisticsPaneLayout.setHorizontalGroup(
            statisticsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statisticsPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(statisticsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(winPercLabel)
                    .addComponent(drawsLabel)
                    .addComponent(wonLabel)
                    .addComponent(lostLabel))
                .addGap(18, 18, 18)
                .addGroup(statisticsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(wonValueLabel)
                    .addComponent(lostValueLabel)
                    .addComponent(drawsValueLabel)
                    .addComponent(winPercValueLabel))
                .addContainerGap(70, Short.MAX_VALUE))
        );
        statisticsPaneLayout.setVerticalGroup(
            statisticsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statisticsPaneLayout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(statisticsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(wonLabel)
                    .addComponent(wonValueLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(statisticsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lostLabel)
                    .addComponent(lostValueLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(statisticsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(drawsLabel)
                    .addComponent(drawsValueLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(statisticsPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(winPercLabel)
                    .addComponent(winPercValueLabel))
                .addGap(0, 25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(loadGameButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(newGameButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(133, 133, 133))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(titleLabel)
                        .addGap(42, 42, 42))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(statisticsPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(65, 65, 65))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(titleLabel)
                .addGap(18, 18, 18)
                .addComponent(newGameButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(loadGameButton)
                .addGap(18, 18, 18)
                .addComponent(statisticsPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void newGameButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newGameButtonActionPerformed
        this.game.initGame();
    }//GEN-LAST:event_newGameButtonActionPerformed

    private void loadGameButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadGameButtonActionPerformed
        game.gotoLoad();
    }//GEN-LAST:event_loadGameButtonActionPerformed

    private GameManager game;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel drawsLabel;
    private javax.swing.JLabel drawsValueLabel;
    private javax.swing.JButton loadGameButton;
    private javax.swing.JLabel lostLabel;
    private javax.swing.JLabel lostValueLabel;
    private javax.swing.JButton newGameButton;
    private javax.swing.JPanel statisticsPane;
    private javax.swing.JLabel titleLabel;
    private javax.swing.JLabel winPercLabel;
    private javax.swing.JLabel winPercValueLabel;
    private javax.swing.JLabel wonLabel;
    private javax.swing.JLabel wonValueLabel;
    // End of variables declaration//GEN-END:variables
}
