package game;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB {
    private final Connection connection;
    private final Statement st;
    private ResultSet res;
    public DB() throws SQLException, ClassNotFoundException{
        Class.forName("com.mysql.jdbc.Driver");
        
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/pao_x0?useSSL=false","root","admin0");
        st = connection.createStatement();
        
        st.execute("create table if not exists saves("
                + "id int not null auto_increment, player char(2),"
                + "config varchar(20), difficulty int, date timestamp,"
                + "name varchar(30) unique, primary key(id))");
        st.execute("create table if not exists statistics"
                + "(won int, lost int, draws int)");
        
        res = st.executeQuery("select * from statistics");
        if (!res.next())
            st.execute("insert into statistics (won,lost,draws)values(0,0,0)");
    }
    
    public int getWon() throws SQLException{
        res = st.executeQuery("select won from statistics");
        if(res.next())
            return res.getInt("won");
        
        throw new SQLException();
    }
    
    public int getLost() throws SQLException{
        res = st.executeQuery("select lost from statistics");
        if(res.next())
            return res.getInt("lost");
        
        throw new SQLException();
    }
    
    public int getDraws() throws SQLException{
        res = st.executeQuery("select draws from statistics");
        if(res.next())
            return res.getInt("draws");
        
        throw new SQLException();
    }
    
    public boolean saveGame(String name,String table,char player,int dif) throws SQLException{
        res = st.executeQuery("select count(id) count from saves where name = '"+name+"'");
        res.next();
        if (res.getInt("count") != 0)
            return false;
        st.execute("insert into saves (name,date,config,player,difficulty)values"
                + "('"+name+"', now(), '"+table+"', '"+player+"', "+dif+")");
        return true;
    }
    
    public void addStat(String stat) throws SQLException{
        int val;
        res = st.executeQuery("select "+stat+" from statistics");
        if(res.next()){
            val = res.getInt(stat);
            st.execute("update statistics set "+stat+" = "+(val+1));
        }
        else
            throw new SQLException();
    }
    
    public String[] getSaves() throws SQLException{
        res = st.executeQuery("select count(id) count from saves");
        res.next();
        String[] v = new String[res.getInt("count")];
        
        res = st.executeQuery("select name from saves");
        int i=0;
        while(res.next())
            v[i++] = res.getString("name");
        
        return v;
    }
    
    public String[] getConfig(String name) throws SQLException{
        res = st.executeQuery("select config,player,difficulty from saves where name = '"+name+"'");
        res.next();
        
        String[] config = new String[3];
        config[0] = res.getString("config");
        config[1] = res.getString("player");
        config[2] = res.getString("difficulty");
        return config;
    }
}
