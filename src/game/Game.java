package game;

import game.interfaces.GameInterface;
import game.interfaces.StartGameInterface;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Created by mihai on 5/13/17.
 */
public class Game {

    public Node<XOState> tree;
    public int difficulty;
    private GameManager game;
    

    public Game(String p,int difficulty,GameManager game)
    {
        tree = new Node<XOState>(new XOState(p));
        this.difficulty=difficulty;
        this.game=game;
        
    }

    public void play() {

                game.gameInterface.menuButton.setEnabled(false);
        game.gameInterface.resetButton.setEnabled(false);
        while (!tree.data.isFinal())
        {
            XOState xo = tree.data;
            if(xo.turn==xo.player)
            {
                movePlayer();
            }
            else
                moveComp();
        }

        game.gameInterface.menuButton.setEnabled(true);
        game.gameInterface.resetButton.setEnabled(true);
        game.gameInterface.saveButton.setEnabled(false);
        if(tree.data.player==tree.data.wonGame())
        {
            game.gameInterface.winLabel.setText("You won!");
            game.addStat("won");
        }
        else if(tree.data.comp==tree.data.wonGame())
        {
            game.gameInterface.winLabel.setText("You lost!");
            game.addStat("lost");
        }
        else{
            game.gameInterface.winLabel.setText("Draw");
            game.addStat("draws");
        }
    }

    public void movePlayer()
    {
        char player = tree.data.player;
        while(!tree.data.move(game.gameInterface.option-1))
        {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        game.gameInterface.putAt(player,game.gameInterface.option-1);
    }

    public void moveComp()
    {
        tree.resetChilds();
        
        game.gameInterface.disableButtons();
        char[] table = tree.data.table;
        expandTree(tree,difficulty);
        tree.permuteChildren();
        tree = getMaxChild(tree);
        
        for(int i=0;i<9;i++)
            if(table[i]!=tree.data.table[i])
            {
                game.gameInterface.putAt(tree.data.comp, i);
                break;
            }
        game.gameInterface.enableButtons();
    }

    public void expandTree(Node<XOState> node,int depth)
    {
        if(depth==0 || node.data.isFinal())
        {
            node.data.score=node.data.tableScore();
            return;
        }

        for(int i=0;i<9;i++)
        {
            XOState xo = new XOState(node.data);
            if(xo.table[i]==' ')
            {
                xo.move(i);
                node.childs.add(new Node<XOState>(xo));
                expandTree(node.childs.get(node.childs.size()-1),depth-1);
            }
        }


        if(node.data.turn==node.data.comp)
            node.data.score=getMaxChild(node).data.score+depth;
        else
            node.data.score=getMinChild(node).data.score-depth;

    }

    public Node<XOState> getMaxChild(Node<XOState> tree)
    {
        int r=0;
        int i;
        for(i=1;i<tree.childs.size();i++)
        {
            if(tree.childs.get(i).data.score > tree.childs.get(r).data.score)
                r=i;
        }

        return tree.childs.get(r);
    }

    public Node<XOState> getMinChild(Node<XOState> tree)
    {
        int r=0;
        int i;
        for(i=1;i<tree.childs.size();i++)
        {
            if(tree.childs.get(i).data.score < tree.childs.get(r).data.score)
                r=i;
        }

        return tree.childs.get(r);
    }

    public void printTable()
    {
        System.out.flush();
        tree.data.printTable();
    }


   
    

}
