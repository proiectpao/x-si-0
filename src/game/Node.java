package game;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by mihai on 5/13/17.
 */
public class Node<T> {

    public T data;
    public ArrayList<Node<T>> childs;
    Random random;
    
    
    public Node(T data)
    {
        random = new Random();
        this.data=data;
        childs=new ArrayList<Node<T>>();
    }
    public Node()
    {
        this.data = null;
        childs=new ArrayList<Node<T>>();
    }
    public void resetChilds()
    {
        childs = new ArrayList<Node<T>>();
    }
    public void permuteChildren()
    {
        for(int i=0;i<childs.size();i++)
        {
            int x,y;
            x = Math.abs(random.nextInt())%childs.size();
            y = Math.abs(random.nextInt())%childs.size();
            Node<T> c1 = childs.get(x);
            Node<T> c2 = childs.get(y);
            
            childs.set(x,c2);
            childs.set(y,c1);
        }
    }
}
