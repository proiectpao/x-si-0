package game;


import game.interfaces.GameInterface;
import game.interfaces.GameMenuInterface;
import game.interfaces.LoadGameInterface;
import game.interfaces.SaveGameInterface;
import game.interfaces.StartGameInterface;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mihai
 */
public class GameManager {
    
    public Game game;
    public JFrame frame;
    public GameInterface gameInterface;
    public StartGameInterface startGameInterface;
    public GameMenuInterface gameMenuInterface;
    public SaveGameInterface saveGameInterface;
    public LoadGameInterface loadGameInterface;
    private DB db;
    public Thread t;
    
    public static void main(String args[])
    {

        GameManager game = new GameManager();

    }
    
    public GameManager()
    {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }

        initFrame();
    }
    
    public void initFrame()
    {
        frame = new JFrame("Tic Tac Toe Minimax");
        updateMenu();
        frame.add(gameMenuInterface);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
    }
        
    public void startGame()
    {
        gameInterface = new GameInterface(this);
        frame.setContentPane(gameInterface);
        System.out.println("Game started!");
        frame.invalidate();
        frame.validate();
        t = new Thread(()->{game.play();});
        t.start();
    }
    
    public void gotoSave(){
        saveGameInterface = new SaveGameInterface(this);
        frame.setContentPane(saveGameInterface);
        frame.invalidate();
        frame.validate();
    }
    
    public void exitSave(){
        frame.setContentPane(gameInterface);
    }
    
    public void exitLoad(){
        frame.setContentPane(gameMenuInterface);
    }
    
    public boolean saveGame(String name,char player,int dif){
        String config = new String(game.tree.data.table);
        try{
            if(db.saveGame(name,config,player,dif))
                return true;
        } catch (SQLException ex) {
            Logger.getLogger(GameManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
  
    public void updateMenu(){
        int won = 0, lost = 0, draws = 0;
        try{
            db = new DB();
            won = db.getWon();
            lost = db.getLost();
            draws = db.getDraws();
        } catch (Exception ex) {
            Logger.getLogger(GameManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        gameMenuInterface = new GameMenuInterface(this,won,lost,draws);
    }
    
    public void gotoMenu()
    {
        updateMenu();
        frame.setContentPane(gameMenuInterface);
        frame.invalidate();
        frame.validate();
    }

    public void initGame()
    {
        startGameInterface = new StartGameInterface(this);
        frame.setContentPane(startGameInterface);
        System.out.println("Choosing game...");
        frame.invalidate();
        frame.validate();
    }
    
    public void addStat(String stat){
        try{
            db.addStat(stat);
        } catch (SQLException ex) {
            Logger.getLogger(GameManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void gotoLoad(){
        
        try {
            loadGameInterface = new LoadGameInterface(this,db.getSaves());
        } catch (SQLException ex) {
            Logger.getLogger(GameManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        frame.setContentPane(loadGameInterface);
        frame.invalidate();
        frame.validate();
    }
    
    public void loadGame(String name)
    {
        String[] config = null;
        try {
            config = db.getConfig(name);
        } catch (SQLException ex) {
            Logger.getLogger(GameManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        gameInterface = new GameInterface(this);
        gameInterface.loadGame(config[0],config[1],Integer.parseInt(config[2]));
        frame.setContentPane(gameInterface);
        System.out.println("Game started!");
        frame.invalidate();
        frame.validate();
        t = new Thread(()->{game.play();});
        t.start();
    }
}
