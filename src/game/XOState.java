package game;

/**
 * Created by mihai on 5/13/17.
 */
public class XOState {

    public static final char X = 'X';
    public static final char O = 'O';
    public char player;
    public char comp;
    public int score;

    public char turn;

    public char[] table;

    public XOState(String player)
    {
        turn = X;
        this.player = player.toUpperCase().charAt(0);
        if(this.player == X)
        {
            comp=O;
        }
        if(this.player == O)
        {
            comp=X;
        }

        table = new char[]{' ',' ',' ',' ',' ',' ',' ',' ',' '};
    }

    public XOState(XOState xos)
    {
        player=xos.player;
        comp=xos.comp;
        turn=xos.turn;
        table=new char[9];

        for(int i=0;i<9;i++)
            this.table[i]=xos.table[i];

    }

    public boolean matchesLine(char x,int x1,int x2,int x3)
    {
        if(table[x1]==table[x2] && table[x2]==table[x3] && table[x3]==x)
            return true;
        return false;
    }

    public char wonGame()
    {
        char[] P = new char[]{'X','O'};
        for(char p : P )
        {
            if(matchesLine(p,0,1,2))
                return p;
            if(matchesLine(p,3,4,5))
                return p;
            if(matchesLine(p,6,7,8))
                return p;
            if(matchesLine(p,0,3,6))
                return p;
            if(matchesLine(p,1,4,7))
                return p;
            if(matchesLine(p,2,5,8))
                return p;
            if(matchesLine(p,0,4,8))
                return p;
            if(matchesLine(p,2,4,6))
                return p;
        }
        return ' ';
    }

    public char changePlayer(char p)
    {
        if(p==X)
            return O;
        if(p==O)
            return X;
        return ' ';
    }

    public boolean move(int pos)
    {
        if(pos>-1 && pos<9 && table[pos]==' ')
        {
            table[pos]=turn;
            turn = changePlayer(turn);
            return true;
        }
        return false;
    }

    public int tableScore()
    {
        if(wonGame()==comp)
            return 10;
        if(wonGame()==player)
            return -10;
        return 0;
    }

    public boolean isFinal()
    {
        if(wonGame()!=' ')
            return true;

        for(char x : table)
        {
            if(x==' ')
                return false;
        }
        return true;
    }

    public void printTable()
    {
        System.out.println("" + table[0] + table[1]+table[2]+ "\t123");
        System.out.println("" + table[3] + table[4]+table[5]+"\t456");
        System.out.println("" + table[6] + table[7]+table[8]+"\t789");
    }

}
